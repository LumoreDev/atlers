import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Renter from '@/components/renter'
import Landlord from '@/components/landlord'
import Service from '@/components/Service'
import PropertyTypes from '@/components/PropertyTypes'
import Contacts from '@/components/Contacts'
import About from '@/components/About'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/renter',
      name: 'Renter',
      component: Renter
    },
    {
      path: '/landlord',
      name: 'Landlord',
      component: Landlord
    },
    {
      path: '/service',
      name: 'Service',
      component: Service
    },
    {
      path: '/property-types',
      name: 'PropertyTypes',
      component: PropertyTypes
    },
    {
      path: '/about',
      name: 'About',
      component: About
    },
    {
      path: '/contacts',
      name: 'Contacts',
      component: Contacts
    }
  ],
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  }
})
